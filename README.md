# Sovol SV07 Plus Klipper config files

This repository contains Klipper config files for my Sovol SV07 Plus 3D printer.

On the printer the config files are located in ~/printer_data/config under the user 'mks'. In this repo, they are under configs.